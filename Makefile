GOPATH ?= $(HOME)/go
PATH := $(GOPATH)/bin:$(PATH)
DESTDIR ?= /

.PHONY: all
all: bins

.PHONY: bins
bins:
	mkdir -p bin
	go build -o bin/installer cmd/installer/*.go
	go build -o bin/installer-gui cmd/installer-gui/*.go

.PHONY: clean
clean:
	rm -rf bin/

.PHONY: deps
deps:
	go get ./...

.PHONY: fmt
fmt:
	find cmd/ internal/ -name '*.go' | xargs gofmt -w -s

.PHONY: vendor
vendor:
	GO111MODULE=on go mod vendor

.PHONY: check
check: goreportcard all test

.PHONY: goreportcard
goreportcard: gofmt govet gocyclo golint ineffassign misspell

.PHONY: gofmt
gofmt:
	gometalinter --deadline=90s --disable-all --vendor ./... -E gofmt

.PHONY: govet
govet:
	go tool vet cmd/ internal/

.PHONY: gocyclo
gocyclo:
	gometalinter --deadline=90s --disable-all --vendor ./... -E gocyclo --cyclo-over 15

.PHONY: golint
golint:
	gometalinter --deadline=90s --disable-all --vendor ./... -E golint --min-confidence=0.85

.PHONY: ineffassign
ineffassign:
	gometalinter --deadline=90s --disable-all --vendor ./... -E ineffassign

.PHONY: misspell
misspell:
	gometalinter --deadline=90s --disable-all --vendor ./... -E misspell

.PHONY: test
test:
	@echo "TODO: we should have some tests"
