// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"flag"
	"gitlab.com/redfield/installer/internal/installer"
	"log"
)

var (
	upgrade      = flag.Bool("upgrade", false, "Upgrade if an existing installation exists.")
	localinstall = flag.Bool("local", false, "Perform a local installation.")
	blockdevice  = flag.String("block-device", "", "Target block device to install redfield to.")
	username     = flag.String("username", "", "User name for remote installation authentication.")
	password     = flag.String("password", "", "Password for remote installation authentication.")
	server       = flag.String("server", "", "Server for remote installation authentication.")
	certificate  = flag.String("certificate", "", "Certificate for remote installation authentication.")
	fdepassword  = flag.String("fde-password", "", "Password for full disk encryption")
	twofactor    = flag.Bool("2fa", false, "Enable token based 2FA for full disk encryption.")
)

func main() {
	flag.Parse()
	log.SetPrefix("[redfield-installer] ")

	var fdeInfo installer.FdeFunctions

	if *twofactor == true {
		fdeInfo = installer.FdeTokenInfo{
			Auth: installer.FdeAuthInfo{
				Password: *password,
				ModeName: "Token",
			},
		}
	} else {
		if *password == "" {
			*password = installer.ProductUuid
		}

		fdeInfo = installer.FdePasswordInfo{
			Auth: installer.FdeAuthInfo{
				Password: *password,
				ModeName: "Password",
			},
		}
	}

	info := installer.InstallInfo{
		BlockDevice: *blockdevice,
		DeviceUuid:  string(installer.ProductUuid),
		Upgrade:     *upgrade,
		RemoteAuth: installer.RemoteInstallAuth{
			Username:    *username,
			Password:    *password,
			Server:      *server,
			Certificate: *certificate,
		},
		FdeInfo: fdeInfo,
	}

	if *localinstall {
		installer.LocalInstall(info)
	} else {
		installer.RemoteInstall(info)
	}
}
